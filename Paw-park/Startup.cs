﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Paw_park.Startup))]
namespace Paw_park
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
