﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Paw_park.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Kasutajanimi")]
        public string UserNamer { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Kasutajanimi")]
        public string UserNamer { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Kasutajanimi")]
        public string UserNamer { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Parool")]
        public string Parool { get; set; }

        [Display(Name = "Jäta kasutaja meelde?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Kasutajanimi")]
        public string UserNamer { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = " {0} peab olema vähemalt {2} tähemärki pikk.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Parool")]
        public string Parool { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita parool")]
        [Compare("Parool", ErrorMessage = "Sisestatud paroolid ei klapi.")]
        public string ConfirmPassword { get; set; }

        // need alumised 4 lisas siia Sandra 
        [Required]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Perekonnanimi")]
        public string LastName { get; set; }
        public string DisplayName { get; set; }
       [DisplayFormat(ApplyFormatInEditMode =true,DataFormatString="{0:dd.mm.yyyy}")]
        [Display(Name = "Sünniaeg")]
        public System.DateTime BirthDate { get; set; }

        // need lisas Kairi
        [Required]
        [EmailAddress]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Telefoni number")]
        public string PhoneNumber { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
       
        [Display(Name = "Kasutajanimi")]
        public string UserNamer { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} tähemärki pikk.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Parool")]
        public string Parool { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita parool")]
        [Compare("Parool", ErrorMessage = "Sisestatud paroolid ei klapi.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = "Kasutajanimi")]
        public string UserNamer { get; set; }
    }
}
