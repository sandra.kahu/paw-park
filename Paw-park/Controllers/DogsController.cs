﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Paw_park.Models;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Paw_park.Models
{

    public partial class Dog
    {
        public static Dictionary<int, string> DogEducLevels = new Dictionary<int, string>
         {
            {1, "Ei allu korrale" },
            {2, "Mitte eriti kuulekas" },
            {3, "Natuke kuulekas" },
            {4, "Üsna kuulekas"},
            {5, "Väga kuulekas"}
          };

        public static Dictionary<int, string> DogSizes = new Dictionary<int, string>
         {
            {1, "Väike <10 kg" },
            {2, "Keskmine >10 kg <25 kg" },
            {3, "Suur <25 kg" }
         };
        public static Dictionary<int, string> DogGenders = new Dictionary<int, string>
        {
            {1,"Poiss" },
            {2,"Tüdruk" }
        };

        public string DogEducLevelName =>
            DogEducLevels.ContainsKey(Educ) ? DogEducLevels[Educ] : "";
        public string DogSizeType =>
            DogSizes.ContainsKey(Size) ? DogSizes[Size] : "";
        public string DogGenderType =>
            DogGenders.ContainsKey(Gender) ? DogGenders[Gender] : "";

        
        public int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));

        public int Getdob => int.Parse(BirthDate.ToString("yyyyMMdd"));
        
        public int DogAge => (now - Getdob) / 10000;

        public string CutValue => (Cut == true) ? "Kastreeritud" : "Kastreerimata";

        public string OffersForDogValue => (OffersForDog == true) ? "Soovin" : "Ei soovi"; 


    }

}



namespace Paw_park.Controllers
{
    public class DogsController : Controller
    {
        private PatrullEntities db = new PatrullEntities();

        // GET: Dogs
        [Authorize]
        public ActionResult Index()
        {
            return View(db.Dogs.ToList());
        }

        // GET: Dogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dog dog = db.Dogs.Find(id);
            if (dog == null)
            {
                return HttpNotFound();
            }
            return View(dog);
        }

        // GET: Dogs/Create
        public ActionResult Create()
        {
            ViewBag.Educ = new SelectList(Dog.DogEducLevels, "Key", "Value");
            ViewBag.Size = new SelectList(Dog.DogSizes, "Key", "Value");
            ViewBag.Gender = new SelectList(Dog.DogGenders, "Key", "Value");
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");

            return View();
        }

        // POST: Dogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Breed,Age,BirthDate,Age,Gender,Cut,Size,Educ,OffersForDog,PictureId")] Dog dog)
        {
            if (ModelState.IsValid)
            {
                db.Dogs.Add(dog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Educ = new SelectList(Dog.DogEducLevels, "Key", "Value");
            ViewBag.Size = new SelectList(Dog.DogSizes, "Key", "Value");
            ViewBag.Gender = new SelectList(Dog.DogGenders, "Key", "Value");
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", dog.PictureId);
            return View(dog);
        }

        // GET: Dogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dog dog = db.Dogs.Find(id);
            if (dog == null)
            {
                return HttpNotFound();
            }

            ViewBag.Educ = new SelectList(Dog.DogEducLevels, "Key", "Value");
            ViewBag.Size = new SelectList(Dog.DogSizes, "Key", "Value");
            ViewBag.Gender = new SelectList(Dog.DogGenders, "Key", "Value");
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", dog.PictureId);
            return View(dog);
        }

        // POST: Dogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Breed,Age,BirthDate,Gender,Cut,Size,Educ,OffersForDog,PictureId")] Dog dog, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dog).State = EntityState.Modified;
                db.SaveChanges();
                // siit hakkab pildi värk
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        // vana pildi kustutamiseks jätame meelde pildi numbri
                        int? vanaPiltId = dog.PictureId;
                        dog.PictureId = df.Id;
                        db.SaveChanges();
                        if (vanaPiltId.HasValue)
                        {
                            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                            db.SaveChanges();
                        }
                    }


                return RedirectToAction("Index");
            }

            ViewBag.Educ = new SelectList(Dog.DogEducLevels, "Key", "Value");
            ViewBag.Size = new SelectList(Dog.DogSizes, "Key", "Value");
            ViewBag.Gender = new SelectList(Dog.DogGenders, "Key", "Value");
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", dog.PictureId);
            return View(dog);
        }

        // GET: Dogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dog dog = db.Dogs.Find(id);
            if (dog == null)
            {
                return HttpNotFound();
            }

            return View(dog);
        }

        // POST: Dogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Dog dog = db.Dogs.Find(id);
            db.Dogs.Remove(dog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
