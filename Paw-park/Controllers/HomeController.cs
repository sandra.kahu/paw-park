﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Paw_park.Models;

namespace Paw_park.Controllers
{
    public class MyController : Controller
    {
        public Person CurrentPerson = null;
        public void CheckPerson()
        {
            if (Request.IsAuthenticated)
                if ((CurrentPerson?.UserName?? "") != User.Identity.Name)
                    using (PatrullEntities db = new PatrullEntities())
                    {
                        CurrentPerson = db.People.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
                        if (CurrentPerson == null)
                        {
                            using (ApplicationDbContext dba = new ApplicationDbContext())
                            {
                                ApplicationUser u = dba.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
                                db.People.Add(CurrentPerson = new Person
                                {
                                    UserName = User.Identity.Name,
                                    FirstName = u.FirstName,
                                    LastName = u.LastName,
                                    BirthDate = u.BirthDate,
                                    Email = u.Email
                                });
                                db.SaveChanges();
                            }
                        }
                    }
                else CurrentPerson = null;
        }
    }

    public class HomeController : MyController
    {
        public ActionResult Index()
        {
            CheckPerson(); // selle panen igale poole, kus vaja personi kontrollida
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}