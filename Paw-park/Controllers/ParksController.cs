﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Paw_park.Models;
using System.IO;

namespace Paw_park.Controllers
{
    public class ParksController : Controller
    {
        private PatrullEntities db = new PatrullEntities();

        // GET: Parks
        public ActionResult Index()
        {
            {
                var departments = db.Parks.Include(d => d.DataFile);
                return View(departments.ToList());


                //string filename = @"C:\Users\siira\source\repos\paw-park3\Paw-park\Koertepargid.txt";
                //string[] pargidtxt = System.IO.File.ReadAllLines(filename);
                //List<Park> pargid = new List<Park>();
                //for (int i = 1; i < pargidtxt.Length; i++)
                //{
                //    string[] reaosad = pargidtxt[i].Split(',');
                //    db.Parks.Add(
                //    new Park
                //    {
                //        Name = reaosad[0],
                //        Location = reaosad[1],
                //    });
                //}


                //this.Bookings = new HashSet<Booking>();
                //this.Comments = new HashSet<Comment>();
                //this.Equipments = new HashSet<Equipment>();

            }
        }

        // GET: Parks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Park park = db.Parks.Find(id);
            if (park == null)
            {
                return HttpNotFound();
            }
            return View(park);
        }

        // GET: Parks/Create
        public ActionResult Create()
        {
            //ViewBag.Name = new SelectList(Park.DogParks, "Key", "Value");

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
                return View();
        }

        // POST: Parks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Location,Type,Capacity,PictureId")] Park park)
        {
            if (ModelState.IsValid)
            {
                db.Parks.Add(park);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.Name = new SelectList(Park.DogParks, "Key", "Value");

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", park.PictureId);
            return View(park);
        }

        // GET: Parks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Park park = db.Parks.Find(id);
            if (park == null)
            {
                return HttpNotFound();
            }
            return View(park);
        }

        // POST: Parks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Location,Type,Capacity,PictureId")] Park park
            , HttpPostedFileBase file)
        {
            string[] vead = db.GetValidationErrors()
                .Select(x => string.Join(",", x.ValidationErrors.Select(y => y.ErrorMessage))).ToArray();


            if (ModelState.IsValid)
            {
                db.Entry(park).State = EntityState.Modified;
                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        // vana pildi kustutamiseks jätame meelde pildi numbri
                        int? vanaPiltId = park.PictureId;
                        park.PictureId = df.Id;
                        db.SaveChanges();
                        if (vanaPiltId.HasValue)
                        {
                            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                            db.SaveChanges();
                        }
                    }

                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", park.PictureId);
            return View(park);
        }

        // GET: Parks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Park park = db.Parks.Find(id);
            if (park == null)
            {
                return HttpNotFound();
            }
            return View(park);
        }

        // POST: Parks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Park park = db.Parks.Find(id);
            db.Parks.Remove(park);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
